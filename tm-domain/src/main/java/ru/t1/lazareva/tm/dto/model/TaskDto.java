package ru.t1.lazareva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.model.IWBS;
import ru.t1.lazareva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity
@Table(name = "tm.tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDto extends AbstractUserOwnedModelDto implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @Nullable
    @Column(nullable = true, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(nullable = true, name = "project_id")
    private String projectId;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

    public TaskDto(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public TaskDto(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public TaskDto(@NotNull final UserDto user) {
        setUserId(user.getId());
    }

    public TaskDto(@NotNull final UserDto user, @NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        setUserId(user.getId());
        setName(name);
        setDescription(description);
        setStatus(status);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}