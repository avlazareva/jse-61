package ru.t1.lazareva.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @SneakyThrows
    @Transactional
    M add(@Nullable String userId, M model) throws Exception;

    @SneakyThrows
    @Transactional
    void clear(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @SneakyThrows
    @Transactional
    void remove(@Nullable String userId, M model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @SneakyThrows
    @Transactional
    M update(@Nullable String userId, M model) throws Exception;
}